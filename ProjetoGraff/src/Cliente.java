
public class Cliente {

	private String nome;
	private Integer idade;
	private Integer idCliente;
	private Lance lances;

	public Cliente(String nome, Integer idade, Integer idCliente) {
		super();
		this.nome = nome;
		this.idade = idade;
		this.idCliente = idCliente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	@Override
	public String toString() {
		return "Cliente [nome=" + nome + ", idade=" + idade + ", idCliente=" + idCliente + ", lances=" + lances + "]";
	}

}
