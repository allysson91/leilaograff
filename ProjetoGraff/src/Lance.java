
public class Lance {

	private long id;
	private Float valor;
	private Cliente cliente;
	private Produto produto;

	public Lance(Float valor, Cliente cliente, Produto produto) {
		this.valor = valor;
		this.cliente = cliente;
		this.produto = produto;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Produto getproduto() {
		return produto;
	}

	public void setLeilao(Produto produto) {
		this.produto = produto;
	}

}
