
public class Leilao {

	private Integer numeroLeilao;

	private Object tipo;

	public Leilao(Integer numeroLeilao) {
		super();
		this.numeroLeilao = numeroLeilao;
	}

	public Integer getNumeroLeilao() {
		return numeroLeilao;
	}

	public void setNumeroLeilao(Integer numeroLeilao) {
		this.numeroLeilao = numeroLeilao;
	}

	@Override
	public String toString() {
		return "Leilao [numeroLeilao=" + numeroLeilao + ", tipo=" + tipo + "]";
	}

}
