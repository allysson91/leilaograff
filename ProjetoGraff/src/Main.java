
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	static ArrayList<Cliente> clientes = new ArrayList<Cliente>();
	static ArrayList<Leilao> leiloes = new ArrayList<Leilao>();

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Model model = new Model();

		int resposta;

		do {

			System.out.println("\n\nDigite o que deseja fazer :" + "\n1 - Cadastro de Usuário (Cliente)"
					+ "\n2 - Cadastro de Produto" + "\n3 - Imprimir Clientes" + "\n4 - Imprimir Produtos"
					+ "\n5 - Dar lance" + "\n6 - Imprimir Lance" + "\n7 - Imprimir Lances por Cliente" + "\n8 - Sair");
			resposta = input.nextInt();

			switch (resposta) {
			case 1:
				model.inserirCliente();
				break;
			case 2:
				model.inserirProduto();
				break;
			case 3:
				model.imprimirCliente();
				break;
			case 4:
				model.imprimirProdutos();
				break;
			case 5:
				model.darLances();
				break;
			case 6:
				model.imprimirLances();
				break;

			case 7:
				System.out.println("Processo Finalizado");
				break;

			default:

				break;
			}
		} while (resposta != 7);
	}

}
