
import java.util.ArrayList;
//import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Model {

	private Scanner le;

	private String nome;
	private Integer numLeilao, id, idade, valor, idCliente;

	private static List<Cliente> clientes;
	private static List<Produto> produtos;
	static List<Leilao> leiloes;
	static List<Lance> lances;

	public Model() {
		clientes = new ArrayList<>();
		leiloes = new ArrayList<>();
		produtos = new ArrayList<>();
		lances = new ArrayList<>();

		le = new Scanner(System.in);

	}

	void darLances() {
		System.out.println("Informe o id do produto: ");
		id = le.nextInt();
		for (Produto produto : produtos) {
			if (!(id == produto.getId())) {
				System.out.println("Produto não encontrado");
			} else {
				System.out.println("Informe o id do cliente: ");
				idCliente = le.nextInt();
				for (Cliente cliente : clientes) {
					if (!(idCliente == cliente.getIdCliente())) {
						System.out.println("Cliente não encontrado");
					} else {
						System.out.println("Informe o valor do lance");
						Float valor = le.nextFloat();
						for (Lance lance : lances) {
							if (valor <= 0) {
								System.out.println("Valor inválido");
							} else if (valor <= lance.getValor()) {
								System.out.println("Valor inválido");
							} else {
								lances.add(new Lance(valor, cliente, produto));
							}
						}
					}
				}
			}
		}
	}

	void inserirCliente() {

		System.out.println("Nome: ");
		nome = le.nextLine();
		System.out.println("Id Cliente: ");
		idCliente = le.nextInt();
		System.out.println("Idade: ");
		idade = le.nextInt();

		clientes.add(new Cliente(nome, idade, idCliente));

		System.out.println("Cadastrado!!!");
	}

	void inserirProduto() {

		System.out.println("Id: ");
		id = le.nextInt();
		System.out.println("Nome: ");
		nome = le.next();
		System.out.println("valor: ");
		valor = le.nextInt();

		produtos.add(new Produto(id, nome, valor));

	}

	void inserirLeilao() {
		System.out.println("Numero do Leilao: ");
		numLeilao = le.nextInt();

		leiloes.add(new Leilao(numLeilao));

	}

	// FUN��ES DE IMPRESS�O

	void imprimirCliente() {
		for (Cliente Cliente : clientes) {
			System.out.println(" | Nome: " + Cliente.getNome() + "  \t| Id: " + Cliente.getIdCliente());
		}

	}

	void imprimirProdutos() {
		for (Produto produto : produtos) {
			System.out.println(
					"ID: " + produto.getId() + " | Nome: " + produto.getNome() + "  \t| Valor: " + produto.getValor());
		}
	}

	void imprimirLeiloes() {
		for (Leilao leilao : leiloes) {
			System.out.println("Número do leilão: " + leilao.getNumeroLeilao());
		}
	}

	void imprimirLances() {

		System.out.println("Insira o id cliente: ");
		idCliente = le.nextInt();

		for (Cliente cliente : clientes) {
			for (Lance lance : lances) {

				if (cliente.getIdCliente().equals(idCliente)
						&& cliente.getIdCliente().equals(lance.getCliente().getIdCliente())) {
					System.out.println("Valor: " + lance.getValor() + " | Cliente: " + lance.getCliente()
							+ " | Produto: " + lance.getproduto());
				}
			}
		}
	}

}
